import pytest


@pytest.mark.parametrize("i", range(50))
def test_num(i):
    if i in {14, 24}:
        pytest.fail("denied")
