FROM python:3.10-alpine

RUN adduser -D sdx
USER sdx
ENV PATH=${PATH}:/home/sdx/.local/bin

RUN pip3 install --user --upgrade pip setuptools pytest
ADD . /opt/sdx
WORKDIR /opt

USER root

RUN /usr/bin/find . -type d -exec chown sdx {} \;
RUN /usr/bin/find /opt/sdx -type f -exec chmod +x {} \;
WORKDIR /opt/sdx
USER sdx

RUN ./ci_test.sh